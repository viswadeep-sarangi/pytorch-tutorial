{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Generating Names with a Character-Level RNN\n",
    "*******************************************\n",
    "**Author**: `Sean Robertson <https://github.com/spro/practical-pytorch>`_\n",
    "\n",
    "In the :doc:`last tutorial </intermediate/char_rnn_classification_tutorial>`\n",
    "we used a RNN to classify names into their language of origin. This time\n",
    "we'll turn around and generate names from languages.\n",
    "\n",
    "::\n",
    "\n",
    "    > python sample.py Russian RUS\n",
    "    Rovakov\n",
    "    Uantov\n",
    "    Shavakov\n",
    "\n",
    "    > python sample.py German GER\n",
    "    Gerren\n",
    "    Ereng\n",
    "    Rosher\n",
    "\n",
    "    > python sample.py Spanish SPA\n",
    "    Salla\n",
    "    Parer\n",
    "    Allan\n",
    "\n",
    "    > python sample.py Chinese CHI\n",
    "    Chan\n",
    "    Hang\n",
    "    Iun\n",
    "\n",
    "We are still hand-crafting a small RNN with a few linear layers. The big\n",
    "difference is instead of predicting a category after reading in all the\n",
    "letters of a name, we input a category and output one letter at a time.\n",
    "Recurrently predicting characters to form language (this could also be\n",
    "done with words or other higher order constructs) is often referred to\n",
    "as a \"language model\".\n",
    "\n",
    "**Recommended Reading:**\n",
    "\n",
    "I assume you have at least installed PyTorch, know Python, and\n",
    "understand Tensors:\n",
    "\n",
    "-  http://pytorch.org/ For installation instructions\n",
    "-  :doc:`/beginner/deep_learning_60min_blitz` to get started with PyTorch in general\n",
    "-  :doc:`/beginner/pytorch_with_examples` for a wide and deep overview\n",
    "-  :doc:`/beginner/former_torchies_tutorial` if you are former Lua Torch user\n",
    "\n",
    "It would also be useful to know about RNNs and how they work:\n",
    "\n",
    "-  `The Unreasonable Effectiveness of Recurrent Neural\n",
    "   Networks <http://karpathy.github.io/2015/05/21/rnn-effectiveness/>`__\n",
    "   shows a bunch of real life examples\n",
    "-  `Understanding LSTM\n",
    "   Networks <http://colah.github.io/posts/2015-08-Understanding-LSTMs/>`__\n",
    "   is about LSTMs specifically but also informative about RNNs in\n",
    "   general\n",
    "\n",
    "I also suggest the previous tutorial, :doc:`/intermediate/char_rnn_classification_tutorial`\n",
    "\n",
    "\n",
    "Preparing the Data\n",
    "==================\n",
    "\n",
    ".. Note::\n",
    "   Download the data from\n",
    "   `here <https://download.pytorch.org/tutorial/data.zip>`_\n",
    "   and extract it to the current directory.\n",
    "\n",
    "See the last tutorial for more detail of this process. In short, there\n",
    "are a bunch of plain text files ``data/names/[Language].txt`` with a\n",
    "name per line. We split lines into an array, convert Unicode to ASCII,\n",
    "and end up with a dictionary ``{language: [names ...]}``.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "# categories: 18 ['Arabic', 'Chinese', 'Czech', 'Dutch', 'English', 'French', 'German', 'Greek', 'Irish', 'Italian', 'Japanese', 'Korean', 'Polish', 'Portuguese', 'Russian', 'Scottish', 'Spanish', 'Vietnamese']\n",
      "O'Neal\n"
     ]
    }
   ],
   "source": [
    "from __future__ import unicode_literals, print_function, division\n",
    "from io import open\n",
    "import glob\n",
    "import os\n",
    "import unicodedata\n",
    "import string\n",
    "\n",
    "all_letters = string.ascii_letters + \" .,;'-\"\n",
    "n_letters = len(all_letters) + 1 # Plus EOS marker\n",
    "\n",
    "def findFiles(path): return glob.glob(path)\n",
    "\n",
    "# Turn a Unicode string to plain ASCII, thanks to http://stackoverflow.com/a/518232/2809427\n",
    "def unicodeToAscii(s):\n",
    "    return ''.join(\n",
    "        c for c in unicodedata.normalize('NFD', s)\n",
    "        if unicodedata.category(c) != 'Mn'\n",
    "        and c in all_letters\n",
    "    )\n",
    "\n",
    "# Read a file and split into lines\n",
    "def readLines(filename):\n",
    "    lines = open(filename, encoding='utf-8').read().strip().split('\\n')\n",
    "    return [unicodeToAscii(line) for line in lines]\n",
    "\n",
    "# Build the category_lines dictionary, a list of lines per category\n",
    "category_lines = {}\n",
    "all_categories = []\n",
    "for filename in findFiles('data/names/*.txt'):\n",
    "    category = os.path.splitext(os.path.basename(filename))[0]\n",
    "    all_categories.append(category)\n",
    "    lines = readLines(filename)\n",
    "    category_lines[category] = lines\n",
    "\n",
    "n_categories = len(all_categories)\n",
    "\n",
    "if n_categories == 0:\n",
    "    raise RuntimeError('Data not found. Make sure that you downloaded data '\n",
    "        'from https://download.pytorch.org/tutorial/data.zip and extract it to '\n",
    "        'the current directory.')\n",
    "\n",
    "print('# categories:', n_categories, all_categories)\n",
    "print(unicodeToAscii(\"O'Néàl\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating the Network\n",
    "====================\n",
    "\n",
    "This network extends `the last tutorial's RNN <#Creating-the-Network>`__\n",
    "with an extra argument for the category tensor, which is concatenated\n",
    "along with the others. The category tensor is a one-hot vector just like\n",
    "the letter input.\n",
    "\n",
    "We will interpret the output as the probability of the next letter. When\n",
    "sampling, the most likely output letter is used as the next input\n",
    "letter.\n",
    "\n",
    "I added a second linear layer ``o2o`` (after combining hidden and\n",
    "output) to give it more muscle to work with. There's also a dropout\n",
    "layer, which `randomly zeros parts of its\n",
    "input <https://arxiv.org/abs/1207.0580>`__ with a given probability\n",
    "(here 0.1) and is usually used to fuzz inputs to prevent overfitting.\n",
    "Here we're using it towards the end of the network to purposely add some\n",
    "chaos and increase sampling variety.\n",
    "\n",
    ".. figure:: https://i.imgur.com/jzVrf7f.png\n",
    "   :alt:\n",
    "\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "import torch\n",
    "import torch.nn as nn\n",
    "\n",
    "class RNN(nn.Module):\n",
    "    def __init__(self, input_size, hidden_size, output_size):\n",
    "        super(RNN, self).__init__()\n",
    "        self.hidden_size = hidden_size\n",
    "\n",
    "        self.i2h = nn.Linear(n_categories + input_size + hidden_size, hidden_size)\n",
    "        self.i2o = nn.Linear(n_categories + input_size + hidden_size, output_size)\n",
    "        self.o2o = nn.Linear(hidden_size + output_size, output_size)\n",
    "        self.dropout = nn.Dropout(0.1)\n",
    "        self.softmax = nn.LogSoftmax(dim=1)\n",
    "        \n",
    "        print(\"i2h Size = {}\".format(i2h.size())\n",
    "        print(\"i2o Size = {}\".format(i2o.size())\n",
    "        print(\"o2o Size = {}\".format(o2o.size())\n",
    "\n",
    "    def forward(self, category, input, hidden):\n",
    "        input_combined = torch.cat((category, input, hidden), 1)\n",
    "        hidden = self.i2h(input_combined)\n",
    "        output = self.i2o(input_combined)\n",
    "        output_combined = torch.cat((hidden, output), 1)\n",
    "        output = self.o2o(output_combined)\n",
    "        output = self.dropout(output)\n",
    "        output = self.softmax(output)\n",
    "        return output, hidden\n",
    "\n",
    "    def initHidden(self):\n",
    "        return torch.zeros(1, self.hidden_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training\n",
    "=========\n",
    "Preparing for Training\n",
    "----------------------\n",
    "\n",
    "First of all, helper functions to get random pairs of (category, line):\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random\n",
    "\n",
    "# Random item from a list\n",
    "def randomChoice(l):\n",
    "    return l[random.randint(0, len(l) - 1)]\n",
    "\n",
    "# Get a random category and random line from that category\n",
    "def randomTrainingPair():\n",
    "    category = randomChoice(all_categories)\n",
    "    line = randomChoice(category_lines[category])\n",
    "    return category, line"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For each timestep (that is, for each letter in a training word) the\n",
    "inputs of the network will be\n",
    "``(category, current letter, hidden state)`` and the outputs will be\n",
    "``(next letter, next hidden state)``. So for each training set, we'll\n",
    "need the category, a set of input letters, and a set of output/target\n",
    "letters.\n",
    "\n",
    "Since we are predicting the next letter from the current letter for each\n",
    "timestep, the letter pairs are groups of consecutive letters from the\n",
    "line - e.g. for ``\"ABCD<EOS>\"`` we would create (\"A\", \"B\"), (\"B\", \"C\"),\n",
    "(\"C\", \"D\"), (\"D\", \"EOS\").\n",
    "\n",
    ".. figure:: https://i.imgur.com/JH58tXY.png\n",
    "   :alt:\n",
    "\n",
    "The category tensor is a `one-hot\n",
    "tensor <https://en.wikipedia.org/wiki/One-hot>`__ of size\n",
    "``<1 x n_categories>``. When training we feed it to the network at every\n",
    "timestep - this is a design choice, it could have been included as part\n",
    "of initial hidden state or some other strategy.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "# One-hot vector for category\n",
    "def categoryTensor(category):\n",
    "    li = all_categories.index(category)\n",
    "    tensor = torch.zeros(1, n_categories)\n",
    "    tensor[0][li] = 1\n",
    "    return tensor\n",
    "\n",
    "# One-hot matrix of first to last letters (not including EOS) for input\n",
    "def inputTensor(line):\n",
    "    tensor = torch.zeros(len(line), 1, n_letters)\n",
    "    for li in range(len(line)):\n",
    "        letter = line[li]\n",
    "        tensor[li][0][all_letters.find(letter)] = 1\n",
    "    return tensor\n",
    "\n",
    "# LongTensor of second letter to end (EOS) for target\n",
    "def targetTensor(line):\n",
    "    letter_indexes = [all_letters.find(line[li]) for li in range(1, len(line))]\n",
    "    letter_indexes.append(n_letters - 1) # EOS\n",
    "    return torch.LongTensor(letter_indexes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience during training we'll make a ``randomTrainingExample``\n",
    "function that fetches a random (category, line) pair and turns them into\n",
    "the required (category, input, target) tensors.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make category, input, and target tensors from a random category, line pair\n",
    "def randomTrainingExample():\n",
    "    category, line = randomTrainingPair()\n",
    "    category_tensor = categoryTensor(category)\n",
    "    input_line_tensor = inputTensor(line)\n",
    "    target_line_tensor = targetTensor(line)\n",
    "    return category_tensor, input_line_tensor, target_line_tensor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Training the Network\n",
    "--------------------\n",
    "\n",
    "In contrast to classification, where only the last output is used, we\n",
    "are making a prediction at every step, so we are calculating loss at\n",
    "every step.\n",
    "\n",
    "The magic of autograd allows you to simply sum these losses at each step\n",
    "and call backward at the end.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "criterion = nn.NLLLoss()\n",
    "\n",
    "learning_rate = 0.0005\n",
    "\n",
    "def train(category_tensor, input_line_tensor, target_line_tensor):\n",
    "    target_line_tensor.unsqueeze_(-1)\n",
    "    hidden = rnn.initHidden()\n",
    "\n",
    "    rnn.zero_grad()\n",
    "\n",
    "    loss = 0\n",
    "\n",
    "    for i in range(input_line_tensor.size(0)):\n",
    "        output, hidden = rnn(category_tensor, input_line_tensor[i], hidden)\n",
    "        l = criterion(output, target_line_tensor[i])\n",
    "        loss += l\n",
    "\n",
    "    loss.backward()\n",
    "\n",
    "    for p in rnn.parameters():\n",
    "        p.data.add_(-learning_rate, p.grad.data)\n",
    "\n",
    "    return output, loss.item() / input_line_tensor.size(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To keep track of how long training takes I am adding a\n",
    "``timeSince(timestamp)`` function which returns a human readable string:\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "import math\n",
    "\n",
    "def timeSince(since):\n",
    "    now = time.time()\n",
    "    s = now - since\n",
    "    m = math.floor(s / 60)\n",
    "    s -= m * 60\n",
    "    return '%dm %ds' % (m, s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Training is business as usual - call train a bunch of times and wait a\n",
    "few minutes, printing the current time and loss every ``print_every``\n",
    "examples, and keeping store of an average loss per ``plot_every`` examples\n",
    "in ``all_losses`` for plotting later.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "torch.Size([1, 18])\n",
      "torch.Size([5, 1, 59])\n",
      "torch.Size([5])\n",
      "torch.Size([5, 1])\n"
     ]
    }
   ],
   "source": [
    "category_tensor, input_line_tensor, target_line_tensor = randomTrainingExample()\n",
    "print(category_tensor.size())\n",
    "print(input_line_tensor.size())\n",
    "print(target_line_tensor.size())\n",
    "\n",
    "print(target_line_tensor.unsqueeze(-1).size())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0m 17s (5000 5%) 2.4715\n",
      "0m 37s (10000 10%) 2.4051\n",
      "0m 57s (15000 15%) 2.3006\n",
      "1m 17s (20000 20%) 2.6681\n",
      "1m 36s (25000 25%) 3.0166\n",
      "1m 53s (30000 30%) 2.7125\n",
      "2m 11s (35000 35%) 2.3977\n",
      "2m 28s (40000 40%) 2.3527\n",
      "2m 46s (45000 45%) 3.1177\n",
      "3m 3s (50000 50%) 1.4389\n",
      "3m 20s (55000 55%) 2.6533\n",
      "3m 37s (60000 60%) 2.1010\n",
      "3m 55s (65000 65%) 2.0625\n",
      "4m 12s (70000 70%) 2.9111\n",
      "4m 29s (75000 75%) 2.0422\n",
      "4m 46s (80000 80%) 2.5578\n",
      "5m 4s (85000 85%) 1.8799\n",
      "5m 21s (90000 90%) 1.9692\n",
      "5m 38s (95000 95%) 2.1345\n",
      "5m 55s (100000 100%) 2.8099\n"
     ]
    }
   ],
   "source": [
    "rnn = RNN(n_letters, 128, n_letters)\n",
    "\n",
    "n_iters = 100000\n",
    "print_every = 5000\n",
    "plot_every = 500\n",
    "all_losses = []\n",
    "total_loss = 0 # Reset every plot_every iters\n",
    "\n",
    "start = time.time()\n",
    "\n",
    "for iter in range(1, n_iters + 1):\n",
    "    output, loss = train(*randomTrainingExample())\n",
    "    total_loss += loss\n",
    "\n",
    "    if iter % print_every == 0:\n",
    "        print('%s (%d %d%%) %.4f' % (timeSince(start), iter, iter / n_iters * 100, loss))\n",
    "\n",
    "    if iter % plot_every == 0:\n",
    "        all_losses.append(total_loss / plot_every)\n",
    "        total_loss = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plotting the Losses\n",
    "-------------------\n",
    "\n",
    "Plotting the historical loss from all\\_losses shows the network\n",
    "learning:\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[<matplotlib.lines.Line2D at 0x3e9e908>]"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    },
    {
     "data": {
      "image/png": "iVBORw0KGgoAAAANSUhEUgAAAX4AAAD8CAYAAABw1c+bAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAADl0RVh0U29mdHdhcmUAbWF0cGxvdGxpYiB2ZXJzaW9uIDIuMi4zLCBodHRwOi8vbWF0cGxvdGxpYi5vcmcvIxREBQAAIABJREFUeJzt3Xl8VNX5+PHPM5N9TyAhISv7KgSILKKiqIhLwV2sWrdW26rVttrW2lrr9/urtmrdqlVbrcvXta64oIhsshMgrIEQAiRASEI2Atkz5/fHXGKWmSwQMmHyvF+veXHnzLkzz9wMz5w599xzxBiDUkqp3sPm6QCUUkp1L038SinVy2jiV0qpXkYTv1JK9TKa+JVSqpfRxK+UUr2MJn6llOplNPErpVQvo4lfKaV6GR9PB+BK3759TUpKiqfDUEqpU8a6desOGWOiO1K3Ryb+lJQU0tPTPR2GUkqdMkRkb0fralePUkr1Mh1O/CJiF5ENIvK5i8f8ReQ9EckWkdUiktLksQes8h0icmHXhK2UUup4dabFfw+Q6eax24BSY8xg4CngrwAiMhKYA4wCZgIviIj9+MNVSil1ojqU+EUkAbgE+LebKrOB163tD4DzRESs8neNMTXGmN1ANjDxxEJWSil1Ijra4n8a+A3gcPN4PJAHYIypB8qBPk3LLfusMqWUUh7SbuIXkUuBQmPMuraquSgzbZS7ep3bRSRdRNKLioraC0sppdRx6kiLfyowS0T2AO8C00Xk/1rU2QckAoiIDxAOlDQttyQAB1y9iDHmZWNMmjEmLTq6Q0NRlVJKHYd2E78x5gFjTIIxJgXnidqFxpgbWlSbC9xkbV9l1TFW+Rxr1M8AYAiwpsuib+HZb3eyJEt/LSilVFuOexy/iDwiIrOsu68AfUQkG/gV8DsAY8xW4H1gG/AVcKcxpuHEQnbvpSW7WKqJXyml2tSpK3eNMYuBxdb2Q03Kq4Gr3ezz/4D/d9wRdkKQvw+VtSfte0UppbyCV125G+Rnp7K23tNhKKVUj+ZliV9b/Eop1R4vS/za4ldKqfZ4YeLXFr9SSrXF6xJ/lSZ+pZRqk1cl/mA/H45qV49SSrXJqxJ/oLb4lVKqXV6V+IP9fThao4lfKaXa4lWJP9DXTlVdAw6Hy3nglFJK4WWJP9jfucZLVZ22+pVSyh2vSvyBfs4ZKHRIp1JKuedViT/Yz9ni14u4lFLKPa9K/EGNiV9b/Eop5Y5XJf7vu3q0xa+UUu54VeIP1ha/Ukq1y6sSf6CV+HUsv1JKuedViT/Y6uqpqtOuHqWUcserEn+QtviVUqpd3pX4/a0Wv/bxK6WUW16V+AN99eSuUkq1x6sSv90mBPjadDinUkq1wae9CiISACwF/K36Hxhj/tSizlPAudbdICDGGBNhPdYAbLYeyzXGzOqi2F3SdXeVUqpt7SZ+oAaYbow5IiK+wDIRmWeMWXWsgjHml8e2ReRuYFyT/auMMaldFnE7gvzsuhiLUkq1od2uHuN0xLrra93amvf4OuCdLojtuOjyi0op1bYO9fGLiF1EMoBC4BtjzGo39ZKBAcDCJsUBIpIuIqtE5LI2XuN2q156UVFRJ95Cc0F+PhzVxK+UUm51KPEbYxqs7poEYKKIjHZTdQ7OcwBNM2+SMSYN+CHwtIgMcvMaLxtj0owxadHR0Z14C805W/za1aOUUu50alSPMaYMWAzMdFNlDi26eYwxB6x/c6x9x7XeresE+enyi0op1ZZ2E7+IRIvIsRE6gcD5wHYX9YYBkcDKJmWRIuJvbfcFpgLbuiZ014L87LoCl1JKtaEjo3rigNdFxI7zi+J9Y8znIvIIkG6MmWvVuw541xjT9MTvCOAlEXFY+z5mjDmpiT/Y387RGu3qUUopd9pN/MaYTbjonjHGPNTi/sMu6qwATjuB+Dot0NdHR/UopVQbvOrKXfh+HH/zHx5KKaWO8b7E72/HYaCm3uHpUJRSqkfyvsRvTdSm3T1KKeWa1yV+fyvxa4tfKaVc877E7+N8S7Wa+JVSyiWvS/x+VuKvqdeuHqWUcsXrEr+/j3b1KKVUW7ww8WuLXyml2uLFiV9b/Eop5YrXJX4/TfxKKdUmr0v8jX38dZr4lVLKFe9L/L7ax6+UUm3xusTvZ9dx/Eop1RavS/zft/g18SullCvel/h1HL9SSrXJCxO/9vErpVRbvC7xax+/Ukq1zesSv80m+Nlt2tWjlFJueF3iB2d3j47jV0op17wy8fv52LSPXyml3Gg38YtIgIisEZGNIrJVRP7sos7NIlIkIhnW7cdNHrtJRHZat5u6+g244u9j0z5+pZRyw6cDdWqA6caYIyLiCywTkXnGmFUt6r1njLmraYGIRAF/AtIAA6wTkbnGmNKuCN4df1+79vErpZQb7bb4jdMR666vdTMdfP4LgW+MMSVWsv8GmHlckXaCv3b1KKWUWx3q4xcRu4hkAIU4E/lqF9WuFJFNIvKBiCRaZfFAXpM6+6yyk8rZx68tfqWUcqVDid8Y02CMSQUSgIkiMrpFlc+AFGPMGGAB8LpVLq6eztVriMjtIpIuIulFRUUdi94N7eNXSin3OjWqxxhTBiymRXeNMabYGFNj3f0XMMHa3gckNqmaABxw89wvG2PSjDFp0dHRnQmrFX8f7eNXSil3OjKqJ1pEIqztQOB8YHuLOnFN7s4CMq3tr4EZIhIpIpHADKvspNI+fqWUcq8jo3rigNdFxI7zi+J9Y8znIvIIkG6MmQv8QkRmAfVACXAzgDGmRET+B1hrPdcjxpiSrn4TLfnpBVxKKeVWu4nfGLMJGOei/KEm2w8AD7jZ/1Xg1ROIsdP8fWzUNmjiV0opV7zyyl1/H7u2+JVSyg2vTPw6ZYNSSrnnlYnfX8fxK6WUW96Z+H11HL9SSrnjnYnfx069w1CvJ3iVUqoVr0z8ftbyizqyRymlWvPKxN+47q6O7FFKqVa8NPHbAW3xK6WUK16a+LXFr5RS7nhl4j/Wx69j+ZVSqjWvTPyNLX4d0qmUUq14Z+L3dfbxa+JXSqnWvDLx+9m1q0cppdzxysTv76tdPUop5Y53Jn4d1aOUUm55aeLXcfxKKeWOlyb+Yy1+7eNXSqmWvDvxax+/Ukq14qWJX4dzKqWUO96Z+K1RPTonv1JKteaViV/H8SullHvtJn4RCRCRNSKyUUS2isifXdT5lYhsE5FNIvKtiCQ3eaxBRDKs29yufgOu2GyCr120q0cppVzw6UCdGmC6MeaIiPgCy0RknjFmVZM6G4A0Y0yliPwM+BtwrfVYlTEmtWvDbp+/j13H8SullAvttviN0xHrrq91My3qLDLGVFp3VwEJXRrlcfDzsWlXj1JKudChPn4RsYtIBlAIfGOMWd1G9duAeU3uB4hIuoisEpHL2niN26166UVFRR0Kvi3hgb6UVdad8PMopZS36VDiN8Y0WN01CcBEERntqp6I3ACkAY83KU4yxqQBPwSeFpFBbl7jZWNMmjEmLTo6ulNvwpV+Yf4cPFx9ws+jlFLeplOjeowxZcBiYGbLx0TkfOBBYJYxpqbJPgesf3Osfccdf7gdFxsWwMFyTfxKKdVSR0b1RItIhLUdCJwPbG9RZxzwEs6kX9ikPFJE/K3tvsBUYFvXhe9ev/AACiuqMca0X1kppXqRjozqiQNeFxE7zi+K940xn4vII0C6MWYuzq6dEOC/IgKQa4yZBYwAXhIRh7XvY8aYbkn8sWEB1DUYSo7W0ifEvzteUimlTgntJn5jzCZcdM8YYx5qsn2+m31XAKedSIDHKzYsAICDh6s18SulVBNeeeUuOLt6AAr0BK9SSjXjtYm/scVfXtNOTaWU6l28NvFHh/ojgg7pVEqpFrw28fvabfQN8adQE79SSjXjtYkfrLH8mviVUqoZr078/fQiLqWUasXLE7+/jupRSqkWvDrxx4YFUFpZR7Uuuq6UUo28OvHHRQQCsL+sysORKKVUz+HVif+0+HAAMnLLPByJUkr1HF6d+IfEhBDq78O63FJPh6KUUj2GVyd+m00YlxzJ+r2a+JVS6hivTvwAE5Ii2VFQweFqXY1LKaWgNyT+5EiM0X5+pZQ6xusT/9jEcGwC6drdo5RSQC9I/KEBvqQmRvDhun1U1tZ7OhyllPI4r0/8AL+7aAT7y6p4flG2p0NRSimP6xWJf+KAKK4YH8/LS3PIL9eLuZRSvVuvSPwAd5w9iLoGw7KdhzwdilJKeVSvSfxDYkKICPJl7Z4ST4eilFIe1W7iF5EAEVkjIhtFZKuI/NlFHX8ReU9EskVktYikNHnsAat8h4hc2LXhd5zNJqQlR7F2j47uUUr1bh1p8dcA040xY4FUYKaITG5R5zag1BgzGHgK+CuAiIwE5gCjgJnACyJi76rgO2vigEh2HzpKYYVO1ayU6r3aTfzG6Yh119e6mRbVZgOvW9sfAOeJiFjl7xpjaowxu4FsYGKXRH4cJg7oA8Da3drqV0r1Xh3q4xcRu4hkAIXAN8aY1S2qxAN5AMaYeqAc6NO03LLPKvOIUf3DCPS1M3fjfm31K6V6rQ4lfmNMgzEmFUgAJorI6BZVxNVubZS3IiK3i0i6iKQXFRV1JKxO87XbuGxcPF9vLeDMxxaRkafTOCilep9OjeoxxpQBi3H21ze1D0gEEBEfIBwoaVpuSQAOuHnul40xacaYtOjo6M6E1Sl/uXw0X917FiEBPjz77c6T9jpKKdVTdWRUT7SIRFjbgcD5wPYW1eYCN1nbVwELjTHGKp9jjfoZAAwB1nRV8MdDRBgeG8atU1NYuL2QrQfKPRmOUkp1u460+OOARSKyCViLs4//cxF5RERmWXVeAfqISDbwK+B3AMaYrcD7wDbgK+BOY0yPWAD3xikphPj78Ni87dTU94iQlFKqW4izYd6zpKWlmfT09JP+Ov+3ai9/+GQLZwzqw79+lEawv89Jf02llDoZRGSdMSatI3V7zZW7rtwwOZm/XzOW1btLuP3NdG35K6V6hV6d+AGuGJ/A364cw/LsYm57LZ0dBys8HZJSSp1UvT7xA1w5IYFHrziNjXllzHxmKSuydSI3pZT30sRvuW5iEt/99lxC/Hz4bFO+p8NRSqmTRhN/ExFBfkwa2Ifl2uJXSnkxTfwtnDWkL7klleQWV3o6FKWUOik08bcwdXBfAJbv0la/Uso7aeJvYVB0MLFhAXybWaDDO5VSXkkTfwsiwoxR/ViQWcjYP89n8Y5CT4eklFJdShO/Cw9eMoIXb5iAn93Gl5t1hI9Syrto4nfB38fOzNGxTBzQhzW7nWv05pdXUVWrXT9KqVOfJv42TBoQxZ7iSrbsL+fcJxYz+dFveX5RtqfDUkqpE6KJvw0TB0QBcN9/N1Jd52BEXCiPf72DXUVH2tlTKaV6Lk38bRjVP4xgPzvbD1ZwzrBo/n5NKgDfbCvwcGRKKXX8NPG3wcduY0KKs9V/8xkp9I8I5LT4cOZvPejhyJRS6vjpBPTtuDYtkRB/O2cPcS4HecHIfjy1IIvFOwrZtK+cmFB/zhvRj+hQfw9HqpRSHdOrF2I5HtsPHmbm0981K7t0TBz/+OF4D0WklFKdW4hFW/ydNKxfKFeOT6BviB93TBvE//sik6+3HqS23oGfj/acKaV6Pk38nSQiPHnN2Mb7F42O5cP1+1i9uxiHgegQf0b2D/NghEop1TZN/Cdo6uC+BPjaeH5RNmv3lBIbFsDC+6bh72P3dGhKKeWS9k2coEA/O2cO7suqnBKCfO3sL6vi3TV5ABytqefJ+TvYfeioh6NUSqnvtZv4RSRRRBaJSKaIbBWRe1zUuV9EMqzbFhFpEJEo67E9IrLZeqxnnrE9QReNjkMEXrhhPJMGRPHcwmwqa+uZu/EAzy3M5qJnlvLe2lxPh6mUUkDHunrqgV8bY9aLSCiwTkS+McZsO1bBGPM48DiAiPwA+KUxpqTJc5xrjPHaCe4vHxfPGYP7EBceiI/NxnX/WsW8zQf5NrOQuPAABseE8NsPN1PvMFw/KdnT4Sqlerl2W/zGmHxjzHpruwLIBOLb2OU64J2uCe/UYLMJceGBAEweGEVSVBDvrMllefYhzh/Rj1duOp3pw2N48OMtrM8t9XC0SqnerlN9/CKSAowDVrt5PAiYCXzYpNgA80VknYjc3sZz3y4i6SKSXlRU1JmwehQR4Yrx8aTvLaWqroHpI2Lw87HxzJxU7Dbh20zndA/VdTrTp1LKMzqc+EUkBGdCv9cYc9hNtR8Ay1t080w1xowHLgLuFJGzXe1ojHnZGJNmjEmLjo7uaFg90hXjEgAI9LUzZWAfAEIDfBkdH87qnBJyiytJfWQ+C3TOH6WUB3Qo8YuIL86k/5Yx5qM2qs6hRTePMeaA9W8h8DEw8fhCPXUk9Qni/BH9uGRMHAG+3w/rnDwgio37ynhnbS7VdQ6W7jx1f9kopU5dHRnVI8ArQKYx5u9t1AsHpgGfNikLtk4IIyLBwAxgy4kGfSr4901pPHH12GZlkwZGUddgeGXZbgA25JZ5IjSlVC/XkVE9U4Ebgc0ikmGV/R5IAjDGvGiVXQ7MN8Y0HbTeD/jY+d2BD/C2Mearrgj8VJSWEoVNoLbeQf/wADLzD1NV20Cgn17spZTqPu0mfmPMMkA6UO814LUWZTnAWFf1e6OwAF9G9g9jx8EKfjVjGPf9dyOb95c3LviilFLdQads6Ga/mD6E/WVVnDPMeQJ7fW6p28SfVVBBv7AAwgN9uzNEpZSX0ykbutmMUbHcMnUAfUP8Se4TxIYm4/qzCiq4778bWZ9bypGaemb9YxnPfrvTg9EqpbyRtvg9aEJyJPM2H2TBtgJW5RTznxV7aHAYio/UcOWEBKrrHGTk6QlgpVTX0sTvQb+5cDjbDhzmx284pzC6bmISIvDe2jxqGxwAbDtwmAaHwW5r9zSLUkp1iCZ+D4oND+D9n07h2QU7OXd4DFMH9yWroIK3V+eyPLuYyCBfSivryCk6wpB+oa32zy+vIjYsAGvUlFJKdYj28XtYWIAvf7h0JFMH9wVgaL9QRsQ5F3K5Y9ogADbvL6esspZPM/bz4pJd1DU4+HDdPqY8upDZzy9nSZZeCKaU6jht8fdAt5yRwguLs/nRlGSeXpDFyl3F/O2rHRw8XA1AaWUtczMOMLBvMIer6rj5P2v47czh3HH2QG39K6XapYm/B7rm9ESuOT0RgJFxYfx33T5E4D+3nM7cjAO8tCQHgLd/MolxiZHc98FGHpu3nUHRIVwwsp8nQ1dKnQK0q6eHGx0fDsCc0xM5d1gMD88aRWJUIOeP6McZg/oS6GfnmWtTiY8I5JVlOR6OVil1KtAWfw93wch+rNtbyn0zhgEQHujL/Hun4Wv/vkvHx27jpjOS+cuX29l6oJxR/cM9Fa5S6hSgLf4e7qwh0Xzxi7PoE+LfWBboZ8fH3vxPd+3pSQT52fn9R5t5Z00ue4uPYoxpVqfgcHWrMqVU76OJ30uEB/ryh0tGkl9ezQMfbWba44u5650NjY9/vukAkx/9lifnZ7Xa90BZlS4Mo1Qvoonfi/xwUhKrf38eC341jSvGx/PFpnyKKmpYnn2IX76Xga/dxsvf5ZBXUtm4z7YDhzn3icVc9eIKSo/WejB6pVR30cTvZUSEwTEh3HbmAAAW7yjksXnbiY8I5Iu7z8QuwmPztgNQUV3Hz99aR2iAD1kFR7jmpZV8sSmfOuuqYaWUd9LE76VGxoURGxbAi0t2sXl/ObdMHcCQfqHcemYKX27J52B5NS8u2UVuSSUvXD+BV25Ko7K2gTvfXs9VL66kwLpm4Jg3V+5h7sYDnnkzSqkupYnfS4kI5w6PYVfRUQJ97Vw+Ph6AK8YnYIyzz/+j9fuZNjSaiQOiOGtINEt/cy7PzEllZ0EFs/6xjIPlzuQ/b3M+f/x0K88saH1+4OkFWfyiybkEpVTPp4nfi503PAaAH4yNIyzAOaf/oOgQRsSF8dzCbPLLq7l8fEJjfbtNmJ0az39/OoXDVfXc/8FGVuw6xH3/3YivXdhVdLTZeYD88ipeWLSLuRsPsHZPCZW19eSXV3Xvm1RKdZomfi921tC+3DA5iTvPHdys/NIxcZRX1RHi78MMF1f6juofzoOXjOC7nYf44b9WEx3qz+NXORdSW99k/YB/Lt6FwxjCA315cv4OrnhhBec+sVjnDlKqh9PE78X8fez872WnkdwnuFn5pWPiALhodCwBvq7X+71+UhI3Tk7m1qkD+PKes7hwVCw+NmHdXmfizy+v4t01eVydlsBtZw5gVU4JOYeOEh8RyE9eT2dVTvHJfXNKqeOmV+72Qsl9gnnpxgmMS4xwW0dE+J/LRjcrG9U/rDHxv2i19n9+zmDCAnzZtK+cG6ckk5oQwcXPfsejX2byyZ1TddI4pXqgdlv8IpIoIotEJFNEtorIPS7qnCMi5SKSYd0eavLYTBHZISLZIvK7rn4D6vhcOCqWmLCATu0zITmKjfvKyCup5J21eVw1IYHEqCDCg3z5901pTBsaTXiQL3dPH8zGfeUszipiV9ERHp2XyW8+2IjDYcgrqeTZb3ficDivIK53MXR0z6Gj3PjKanYWVHTJe1VKNdeRrp564NfGmBHAZOBOERnpot53xphU6/YIgIjYgeeBi4CRwHVu9lWngAnJkVTXObj0uWU4HKbVuYNjrhifQEJkIHe8sY7znlzCS0tyeD99H5v3l/Ov73L4+zdZbNpfzoJtBYz583wKWwwdffKbLL7beYj7PthEg+P7KSbqGhw8Nm+7nkBW6gS1m/iNMfnGmPXWdgWQCcR38PknAtnGmBxjTC3wLjD7eINVnnXeiBjuOW8IZw7uywMXjyAxKshlPT8fG4/MHsW0YdE8/IORfH3v2dgEvs0sYMG2AgCWZhXxccZ+KmsbWLW7pHHfrIIKPt90gLGJEWzMK+M/y3c3PrYht4wXl+zi3TV5J/eNKuXlOtXHLyIpwDhgtYuHp4jIRuAAcJ8xZivOL4im/0v3AZOOK1LlcQG+dn55wdAO1Z0+vB/Th38/Ymh8UiRvrtpLaWVd45fArqKjAKzfW8qssf2pqm3gz59tJcjXzms3n85d76znlWW7ue3MAYhI44iilTnF/NLFa+aXV7Eht4yLT4s74fcK0OAw1Dsc+Pu4PgGu1Kmqw6N6RCQE+BC41xhzuMXD64FkY8xY4Dngk2O7uXgql9NDisjtIpIuIulFRToc0NtMHxFDaWUdIs6ZRDfuK+dITT1BfnbW55ZSXlnHnJdXsmJXMX+4dCSRwX7MHhtPfnk1mfnOvv711onlDbmlVNU2n1Sutt7Bj19P5+dvrWftnpJWr388/vb1dmY9t7xLnkupnqRDiV9EfHEm/beMMR+1fNwYc9gYc8Ta/hLwFZG+OFv4iU2qJuD8RdCKMeZlY0yaMSYtOjq6k29D9XTnj3C2/ickRXL5OGdPYaCvnesmJrH1wGGeWuDs93/phglcNzEJgHOGOz8Hi3YUYoxhfW4ZsWEB1DUY0vc2T+7PLdzJ1gOHCfS18+y3O6mormPL/vITinnBtgJ2FFRwpKb+hJ5HqZ6mI6N6BHgFyDTG/N1NnVirHiIy0XreYmAtMEREBoiIHzAHmNtVwatTx5CYEGaN7c8tUwcwLimCsAAfpg2NZurgPjQ4DK+v3MPFo+OYMSq2cZ+Y0ADGJITzbWYB+0qrOHSkhpunpuBjE1bucl4nUN/g4NF5mTy3MJurJiRw7/lD+G7nIaY9vphLn1vGxryy44q3qKKmsStqt/WvUt6iI338U4Ebgc0ikmGV/R5IAjDGvAhcBfxMROqBKmCOca74US8idwFfA3bgVavvX/UyIsKz141rvP/eHVPoE+KHr83Z9jAGfnbOoFb7TR8ewzPf7uTLzfkAnDm4Lwu2FbB0ZxH3XziMP3+2jTdX7eX6SUn88dKRzi+RFXuIDvXHYQzPfLuTV28+vc3Yjg0pbbq4TdPuopxDRzgtQVc1U96j3cRvjFmG6776pnX+AfzDzWNfAl8eV3TKa42IC2vcHh3vnEn02PrCTc0cHctzC7N5dN52gvzsDI8NZXZqf/746VYe+Ggz767N4+YzUnh41qjGfZb85lx8bMLzi7J5Yn4Wm/aVMSYhgrySSoL87IQG+HLX2+vx87Hx6BWn8aNX11B4uIanrk1l4oAoAFbnFBPoa6e6voGcTrb4V+4qJibMn0HRIcd5dJQ6uaQnLsWXlpZm0tPTPR2G6iaHq+vws9vcTh+xZX85H6zbR/+IAG4/exAOh+HHb6SzcHshceEBLPjVNIL9W7dhKqrrOOtviwjx9+HmM1J4Yv4OAnztjEmIYKk1n1DfEH+Kj9YQFxbAwcPVvH/HFNJSopj59FKiQ/3ZU3yU1MRInmvya6Utr6/Yw5/mbmXa0Ghev3Viq8fLK+v4/ceb+c3MYY1Taby4ZBcrdhXzhov6SnWUiKwzxqR1pK7O1aM8LizA123SBxgdH87Ds0Zx+9nOriCbTXjy6rFMHx7DE1ePdZn0AUIDfHntlonU1jv43y8yGREXRnxEIEuzivjFeUO4b8ZQDh2p4YGLhvP1L88mxN+Ht1fnUlRRw46CCiamRDGwbwg5RUdaPfe+0kp++8Emvtv5/Qi0ZTsP8ae5W/Gz28jMbznwzemTjP18sTmffyzMBqCmvoGXl+awNKuocRpspU42natHnZIig/3a7bsHSE2M4LO7z2T+tgKunpCACGzMK+f0lEhEhKvTEulnTV1xyZg4Ps04QFigcwrri06Lo6SylrV7SjDGNM47tCSriJ++uY6qugY25JXy9b1nIyKs3l2M3SbcPX0wT36TRcnRWqKC/ZrF8/GG/QB8mnGA+2cOY83uEkqsqa5X7y5mdqrrayOP1tSzs/AIqU3mV/rjJ1tIigriJ2cP7OTRU72dtviV1+sXFsCNk5MJ8LXj72Nn4oCoxiTer8l8RZelxlNZ28BrK/ZwyWlxDI4JYWB0CJW1DRy0ppVocBj+5/NtxEUEcN+MoWQVHGF5tnOEUWZ+BQP6BjPWSs7bDzZv9ecUHSEjr4wfTkqizuHgmQU7eXPlXvqHBxAa4ON2RtPCimqufnEllz2/nEU7ChvP+NCHAAATOklEQVSf+81Ve3l6QRaHq+u65DjN25zP3mIdwdQbaOJXynJ6ShTxEYEA3D19CACD+jr74XcVOhPil5vzyS48wq8uGMqPzxpI3xC/xmkldhQcZlhsKMNjQ533D1bwyGfb+Omb68gurOBf3+1GBO45bwg/GNOft1bnsnp3CdecnsikAVGsyml+bcKW/eXc9Ooazn9yCbsPHSUhMpAHP9rMkZp6Xlu+B1+7cLS2gffXtj2FhTGG+VsPNl70Nm9zPmWVtc3qrNh1iJ+9tZ6/f9N6lTXlfbSrRymLzSY8cPFwcksqGWYl78ExzpE5P3p1NbFhARytbWBITAgXj47DZhN+ODGJ5xZlk114hLySKq6ZkEh0qD9RwX58t/MQS7KKaHAYvtp6EIDZqf3pFxbAk9eM5aYzktlbXMnM0bG8vTqXBZmFHCyvJjY8gOq6Bu56ez0V1fVcOCqWm85IobbBwZX/XMG1L60ku/AIV01IZFfhEf6zfA/XTUxi3d5SXl6aw8OzRjI4JrTxfc3bcpCfv7We+y8cxrnDYvjZW+u5/eyB/P7iEQBU1zXw+482A7BiV3Gzbq26BgcPfryZUf3DuemMlFbHrOBwNZc/v5ynrk1l0sA+J+1vo7qWJn6lmrh0TP9m92PCAnjxhglsO1DOvrIqCg/X8LNzBmGzORPjrNR4nl2YzQuLnCdrh8aGIiIM6xfKwu3Obpk3bp3IxrwyJg3sw+kpkQD42m1MSI5iQrJz+OikAc6keayf/6kFWewpruTtn0zijEF9G+N56ppUnl24E4cx3Do1hbzSSm59LZ3Jf/mWI7X1GAN3v5PBJ3eegb+PnboGB49/vQOAzzflU1nrvAr5660HeeCi4YgIryzbzZ7iSi5L7c8nGQfYWXiEof1CMcbwh4+38H76PiKDCrhuYhJ+Ps07CVblFHOgvJon5u/g/Tum6PoLpwhN/Eq1Y+boWGaOjnX5mPM8QDCfbnTORHKsm2d4XCgrc4qZkBzJ2UOjOXto29OQjOwfRpCfnQ25ZUwfHsMr3+3m6gkJzZI+wGXj4pmd2p+yyjoig/0Y0i+Uj35+Bq+v2EPfEH/GJUVw19sbuOalVfQJ9qOqtoHdh44ybWg0S7KKOFhehZ+Pjb3FlWw/WMHgmBBeX7GHs4b05dczhvFJxgGWZx8iPiKQP366hY/W7+esIX35buchFm4vbHUcjk2LsXZPKat3lzDZavVX1tbz4fr9XHJaXKsT3CfiqW+yyCqo4J83THBbp/RoLfvLqlxeF6KctI9fqRN0wch+NDgMQX52EiOdU1Uf+wK4ekJCW7s2stuE0fHhZOSVsSG3jHqHYVZqf5d1RYTIJsl0fFIkz8wZxx8vHcmlY/pz/4XDqK13UFhRTcHhaman9uevV45BBEor6/jF9MGIwFdbDjJvy0EKK2q4deoAEqOCSIoKYu7GA8x+fjkfb9jPPecN4dWbTyc61J8P1+9rFcvm/eUMjw0lOtSfv3yZSenRWlbsOsTFz3zHHz/Zwv+t2ttYNyOvjEc+28aJXDv00YZ9zN9W0GqSvmOWZBUx4+mlXPb88sbRUi29vHQX86wrwU9E6dHaxpPtpxpt8St1gmaMjOWlJTkM6Rfa2AU0c3Qc+0qr3A7PdCU1MYLXlu9hVU4xNqHZ0M3OuPPcwS4XyTk9OYp1uaVcPymZpVmHeGdNLgG+dgb0DWaa9Ytk6uC+vLMml4ggX966bRJnDHb+4rh8XDyvLttNXkll4zoMDodh6/7DzB7Xn6mD+nLPuxlMe3wRh6vrSYwKJCrYjyxrFTVjDA99uqVxic4BfYNbxdeevJJK8kqci/Bs3l/eeJX1MZn5h7nttbX0CfGj3mFYs7uYmaObT9G9YFsBf/lyO4Oig7noOKbv/s/y3fzfqr18de/ZPL0gizdW7WX9Hy5o9kV8KtAWv1InaFxiBIlRgUxIimwsCw/05dczhhHo1/G5/FMTI6htcPB++j6GxYYRGuDbpXE+eMkI/nblGCKD/fjxWQMIC/TlaE09d08f3PiFdf2kJM4fEcPHP5/amPQBbpiUTKCfnRtfWd24Ytrekkoqauo5LT6ci06L4707JjMgOoRfXzCUb345jXGJEewscF78tiSriE37nN1Cx9ZtdqW23kG6dd1ESyubDHfdkNv8ORochgc+2kx4oC+f3X0mgb72VqOkSo7W8ruPNuNnt7Gr6Cg5RUd4flE2N76ymvv/u5En5+9oXPPBnQ/X72NX0VGWZx/i660FGAPbD556S4Rqi1+pE2SzCV/84iwCTnDBlmPj/w8dqeEiN+cUTvT5j73GjFGxzWZCPWZ0fDj/vqn1hXFJfYJ47ZaJ3PjKas55YjEzR8cy0ppv6Vhf+rikSD69c2rjPkP6hbJ0ZxH1DQ7+sTCb/uEBVNTUs25vKVc16QIzxrCr6ChJUUHc8+4G5m05yF+vPI1rT09qNsJo5a5i+gT7Eezvw4bc5rOu/mNhNhl5ZTx9bSoxoQFMSI5kVU4xeSWVfJqxnxsmJ3PPuxs4XFXHC9eP58dvpPPKst28uzaP2LAAsgoqKKqo4bONB1h8/7kuj9/B8mq27Hdem/H41zsar+3IzD/MlEEdH9HkcBgcxuBjt3GwvJqK6jqG9Attf8cupIlfqS4Q1gWt8/7hAfQN8efQkRomJEe2v0M3m5AcyYc/O4M3Vu7h4w37+Wj9fvzsNoa6SVpD+4VQ12BYurOI9L2lPHjxCJZlH2L93lIW7SjkmQU7ef3WiXyasZ+HPt1KsJ+do7UNxIT689evdlBytI4XFmXz03MG8bNpg1i5q5jJg/rga5PG1rwxhsfmbeelpTlcltqf2dZ5kckDo3hifhZ3vLmObfmH+efiXRytbeCxK07j/JH9GBEXxlurc/H3sfHxnWcQExrAv7/L4X+/yCS/vIrVOSVk5JXxwMXDOVJdT2llXeOMrceWBbXbhCA/e6sL9dpz9zsbyC2p5J3bJ3PjK6spPlrLit9Nb3Pakq6miV+pHkJESE0MZ0FmYY9M/OCcVfXRK8Zww+RkfvJ6OgOig/G1u+4xHmJdS/DikhwAZozqR1VdA08tyOJPn24lt6SSF5fsYm7GAYbHhjI4JoTUxAimDOrDD55bxl+/2k5SVBCPf72DFxZlc7S2gSkD+1Df4OCTjAPkl1eRkVvGS0tzuH5SEo/MHt346+DY6KJt+Yf5+TmD+DazkCmD+jDHWuTnghExZOYf5oeTkogJDWi2z6qcYp74Oov9ZVVs3FfGrsIjVNU1EB8RSHxEIPeeP4Rb/rOWiSlR2G3SuEJcWx76dAs2cU7n8dXWgzQ4DLOeW0bOoe8vDLxifMcGAnQFTfxK9SCXj0vAx2YjITLQ06G0aVT/cBbdfw71De5H6AyOCUEE1uwuYVB0MMl9gpmQHIkxkFtSSXKfIP65eBcAr9yUxnkjvl+j+eFZo6iqbeAnZw1k7sYDrM8tpU+wP7NT+zdOk/30NztZnFXIqP5h/HnWKOy2768hGJMQQWiAD+OTIrn/wmH8ZubwZrFdNSGRzfvLm60BMSIujNAAH15eupv9ZVVcMLIfCzILOD0lCofDkL63lB9NSebMwX2ZmBLFDZOTycgr5Y2Ve6lvcDRbzwGgrLIWX7sNEXh3bR51DQ7qGhw0OEzjENmLT4tl+8EK3ly1VxO/Ur3VJWPiuGRM1ywWf7L5+9hxMzEqAIF+dhIiA8krqWL68BjA2U1iE+d5gSevHsuFTy9lcEwI5w6Labbvj6akNG5fNi6ey8Z9PzpqTEI4P5qSzBsr92IT+NeP0lolXT8fG5/ddSYxYf4uLypL6hPEf25pPg223SZMGhDFgsxC/Ow2nrxmLLX1DqKC/Kipd/Dv73K4YkICvnYb7/90CuC86rmm3sGe4koG9g3mrnfWExMawKzU/tz86hrGJkZww+RkausdiMBbq3MZERfGyzem8eaqPVw1IZFPNuznkc+3sWh7IecOj2kV68mgiV8pddIMjQklr6SqMaGF+PvwzJxxjIgLY3BMCE9dm0pKn+DGUUUdISI8Mns0l42Lp/RoLWMSXA97TTmOIaOTB/ZhQWYh04ZFNztvE+hn5+7zhrSqf2xBocz8w6zfW8qXm51Tc7y2Yg8+NuG7nYeoqK4nNMCHa9MS+fey3VwxLp5AP3vjNONXpSXw5qq93PLa2saV5E52f78mfqXUSTMhJZItB8o5PeX7Mfc/GPv9hWmduc6hpfFJXX8e5Kwh0YhkcsW4jsU1KCYYP7uNfy7eRWFFNeOTIvjJWQN5Lz2P+2YM49qXVpKRV8alY+K4e/oQDHBNWmKz5wgL8OWLX5zJ3+dnsXFfmdtzJl1JV+BSSp00Doehpt7RqesZPO1AWRX9Izp+juXTjP08+uV2Ciuq+eTOqc1+gfz5s638Z/kenrp2LJePa78Pv67BcdyJvzMrcGmLXyl10thsckolfaBTSR+cv1ouHBVLweHqxuU0j/n5Oc4rqC90cc2EK93R2gdN/EopdcICfO2tkj5AdKg/f/rBKA9E1LZ2v15EJFFEFolIpohsFZF7XNS5XkQ2WbcVIjK2yWN7RGSziGSIiPbfKKWUh3WkxV8P/NoYs15EQoF1IvKNMWZbkzq7gWnGmFIRuQh4GZjU5PFzjTGHui5spZRSx6vdxG+MyQfyre0KEckE4oFtTeqsaLLLKqD7rkRQSinVKZ06kyAiKcA4YHUb1W4D5jW5b4D5IrJORG5v47lvF5F0EUkvKirqTFhKKaU6ocMnd0UkBPgQuNcY43JWIhE5F2fiP7NJ8VRjzAERiQG+EZHtxpilLfc1xryMs4uItLS0njfGVCmlvESHWvwi4osz6b9ljPnITZ0xwL+B2caYxomzjTEHrH8LgY+Bia72V0op1T06MqpHgFeATGPM393USQI+Am40xmQ1KQ+2TggjIsHADGBLVwSulFLq+HSkq2cqcCOwWUQyrLLfA0kAxpgXgYeAPsAL1oRI9dYVZP2Aj60yH+BtY8xXXfoOlFJKdUqPnLJBRIqAve1WdK0v0BOHjmpcnddTY9O4Okfj6rzjiS3ZGBPdkYo9MvGfCBFJ7+h8Fd1J4+q8nhqbxtU5GlfnnezYdLF1pZTqZTTxK6VUL+ONif9lTwfghsbVeT01No2rczSuzjupsXldH79SSqm2eWOLXymlVBu8JvGLyEwR2SEi2SLyOw/G4XIaaxF5WET2W9NTZ4jIxR6Kr9U02SISJSLfiMhO69+uX9Ou7ZiGNTkuGSJyWETu9cQxE5FXRaRQRLY0KXN5fMTpWeszt0lExnsgtsdFZLv1+h+LSIRVniIiVU2O3YvdHJfbv52IPGAdsx0icmE3x/Vek5j2HLs2qZuPl7sc0X2fM2PMKX8D7MAuYCDgB2wERnooljhgvLUdCmQBI4GHgft6wLHaA/RtUfY34HfW9u+Av3r4b3kQSPbEMQPOBsYDW9o7PsDFOCckFGAysNoDsc0AfKztvzaJLaVpPQ/E5fJvZ/1f2Aj4AwOs/7f27oqrxeNPAg954Hi5yxHd9jnzlhb/RCDbGJNjjKkF3gVmeyIQY0y+MWa9tV0BHJvGuiebDbxubb8OXObBWM4DdhljjvcCvhNinBMIlrQodnd8ZgNvGKdVQISIxHVnbMaY+caYeuuuR6ZEd3PM3JkNvGuMqTHG7AayOUnzd7UVlzUVzTXAOyfjtdvSRo7ots+ZtyT+eCCvyf199IBkK62nsb7L+qn2and3pzThaprsfsa57gLWvzEeig1gDs3/M/aEY+bu+PS0z92tNJ8SfYCIbBCRJSJylgficfW36ynH7CygwBizs0lZtx+vFjmi2z5n3pL4xUWZR4crSetprP8JDAJScS5s86SHQptqjBkPXATcKSJneyiOVkTED5gF/Ncq6inHzJ0e87kTkQdxrpb3llWUDyQZY8YBvwLeFpGwbgzJ3d+upxyz62jewOj24+UiR7it6qLshI6ZtyT+fUBik/sJwAEPxeJyGmtjTIExpsEY4wD+hYempzaup8kuOPbT0fq30BOx4fwyWm+MKbBi7BHHDPfHp0d87kTkJuBS4HpjdQpbXSnF1vY6nH3pQ7srpjb+dh4/ZiLiA1wBvHesrLuPl6scQTd+zrwl8a8FhojIAKvVOAeY64lArL7DVtNYt+iTuxwPTE8t7qfJngvcZFW7Cfi0u2OzNGuF9YRjZnF3fOYCP7JGXUwGyo/9VO8uIjIT+C0wyxhT2aQ8WkTs1vZAYAiQ041xufvbzQXmiIi/iAyw4lrTXXFZzge2G2P2HSvozuPlLkfQnZ+z7jiL3R03nGe+s3B+Uz/owTjOxPkzbBOQYd0uBt4ENlvlc4E4D8Q2EOeIio3A1mPHCeeU2t8CO61/ozwQWxBQDIQ3Kev2Y4bziycfqMPZ0rrN3fHB+RP8eesztxlI80Bs2Tj7f4991l606l5p/Y03AuuBH3RzXG7/dsCD1jHbAVzUnXFZ5a8BP21RtzuPl7sc0W2fM71yVymlehlv6epRSinVQZr4lVKql9HEr5RSvYwmfqWU6mU08SulVC+jiV8ppXoZTfxKKdXLaOJXSqle5v8D0riTYnwRSt4AAAAASUVORK5CYII=\n",
      "text/plain": [
       "<Figure size 432x288 with 1 Axes>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.ticker as ticker\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(all_losses)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sampling the Network\n",
    "====================\n",
    "\n",
    "To sample we give the network a letter and ask what the next one is,\n",
    "feed that in as the next letter, and repeat until the EOS token.\n",
    "\n",
    "-  Create tensors for input category, starting letter, and empty hidden\n",
    "   state\n",
    "-  Create a string ``output_name`` with the starting letter\n",
    "-  Up to a maximum output length,\n",
    "\n",
    "   -  Feed the current letter to the network\n",
    "   -  Get the next letter from highest output, and next hidden state\n",
    "   -  If the letter is EOS, stop here\n",
    "   -  If a regular letter, add to ``output_name`` and continue\n",
    "\n",
    "-  Return the final name\n",
    "\n",
    ".. Note::\n",
    "   Rather than having to give it a starting letter, another\n",
    "   strategy would have been to include a \"start of string\" token in\n",
    "   training and have the network choose its own starting letter.\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Roshin\n",
      "Uanten\n",
      "Shantov\n",
      "Gerren\n",
      "Eren\n",
      "Ronn\n",
      "Santer\n",
      "Para\n",
      "Allan\n",
      "Can\n",
      "Han\n",
      "Iung\n"
     ]
    }
   ],
   "source": [
    "max_length = 20\n",
    "\n",
    "# Sample from a category and starting letter\n",
    "def sample(category, start_letter='A'):\n",
    "    with torch.no_grad():  # no need to track history in sampling\n",
    "        category_tensor = categoryTensor(category)\n",
    "        input = inputTensor(start_letter)\n",
    "        hidden = rnn.initHidden()\n",
    "\n",
    "        output_name = start_letter\n",
    "\n",
    "        for i in range(max_length):\n",
    "            output, hidden = rnn(category_tensor, input[0], hidden)\n",
    "            topv, topi = output.topk(1)\n",
    "            topi = topi[0][0]\n",
    "            if topi == n_letters - 1:\n",
    "                break\n",
    "            else:\n",
    "                letter = all_letters[topi]\n",
    "                output_name += letter\n",
    "            input = inputTensor(letter)\n",
    "\n",
    "        return output_name\n",
    "\n",
    "# Get multiple samples from one category and multiple starting letters\n",
    "def samples(category, start_letters='ABC'):\n",
    "    for start_letter in start_letters:\n",
    "        print(sample(category, start_letter))\n",
    "\n",
    "samples('Russian', 'RUS')\n",
    "\n",
    "samples('German', 'GER')\n",
    "\n",
    "samples('Spanish', 'SPA')\n",
    "\n",
    "samples('Chinese', 'CHI')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Exercises\n",
    "=========\n",
    "\n",
    "-  Try with a different dataset of category -> line, for example:\n",
    "\n",
    "   -  Fictional series -> Character name\n",
    "   -  Part of speech -> Word\n",
    "   -  Country -> City\n",
    "\n",
    "-  Use a \"start of sentence\" token so that sampling can be done without\n",
    "   choosing a start letter\n",
    "-  Get better results with a bigger and/or better shaped network\n",
    "\n",
    "   -  Try the nn.LSTM and nn.GRU layers\n",
    "   -  Combine multiple of these RNNs as a higher level network\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python [conda env:pytorch]",
   "language": "python",
   "name": "conda-env-pytorch-py"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
